<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Page-->
    <title>SapoStore</title>

    <!-- Icons font CSS-->
    <link href="{!! asset('html/register/vendor/mdi-font/css/material-design-iconic-font.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('html/register/vendor/font-awesome-4.7/css/font-awesome.min.css') !!}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{!! asset('html/register/vendor/select2/select2.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('html/register/vendor/datepicker/daterangepicker.css') !!}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{!! asset('html/register/css/main.css') !!}" rel="stylesheet" media="all">
</head>

<body>
<div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
    <div class="wrapper wrapper--w780">
        <div class="roof_store">
            <img src="{!! asset('html/gentelella-master/production/images/roof-store.png') !!}" width="900"/>
        </div>
        <div class="card card-3">
            <div class="card-heading"></div>
            <div class="card-body">
                <h2 class="title">
                    Dùng thử Sapo miễn phí 15 ngày<br>
                    +67,000 doanh nghiệp & chủ shop tin dùng
                </h2>
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="Họ tên của bạn" name="name">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="text" placeholder="Số điện thoại của bạn" name="phone">
                    </div>
                    <div class="input-group form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input class="input--style-3" type="text" placeholder="Email đăng ký" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="password" placeholder="Mật khẩu đăng nhập cửa hàng của bạn" name="password">
                    </div>
                    <div class="input-group">
                        <input class="input--style-3" type="password" placeholder="Nhập lại mật khẩu" name="password_confirmation">
                    </div>
                    <div class="p-t-10">
                        <button class="btn btn--pill btn--green" type="submit">Đăng ký</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button class="btn btn--pill btn--green"><a href="/" style="text-decoration: none;color:black;">Quay lại</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery JS-->
<script src="{!! asset('html/register/vendor/jquery/jquery.min.js') !!}"></script>
<!-- Vendor JS-->
<script src="{!! asset('html/register/vendor/select2/select2.min.js') !!}"></script>
<script src="{!! asset('html/register/vendor/datepicker/moment.min.js') !!}"></script>
<script src="{!! asset('html/register/vendor/datepicker/daterangepicker.js') !!}"></script>

<!-- Main JS-->
<script src="{!! asset('html/register/js/global.js') !!}"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
<style>
    .has-error{
        color:red;
    }
    .roof_store{
        position: absolute;
        top: 85px;
        z-index: 9;
        right: 353px;
    }
</style>
</html>
<!-- end document-->