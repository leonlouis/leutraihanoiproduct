<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Login V12</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/vendor/bootstrap/css/bootstrap.min.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/vendor/animate/animate.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/vendor/css-hamburgers/hamburgers.min.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/vendor/select2/select2.min.css') !!}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/css/util.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('html/login/css/main.css') !!}">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('{!! asset('html/login/images/img-01.jpg') !!}');">
        <div class="wrap-login100 p-b-30">
            <form class="form-horizontal login100-form validate-form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="login100-form-avatar" style="margin-bottom: 45px">
                    <img src="{!! asset('images/favicon.ico') !!}" alt="AVATAR">
                </div>
                <div class="wrap-input100 validate-input m-b-10 form-group{{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "Nhập địa chỉ email">
                    <input class="input100" type="text" name="email" placeholder="Địa chỉ email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
                </div>
                <div class="wrap-input100 validate-input m-b-10 form-group{{ $errors->has('password') ? ' has-error' : '' }}" data-validate = "Nhập mật khẩu">
                    <input class="input100" type="password" name="password" placeholder="Mật khẩu">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
                </div>

                <div class="container-login100-form-btn p-t-10">
                    <button class="login100-form-btn" type="submit">
                        Đăng nhập
                    </button>
                </div>
                <div class="text-center w-full p-t-25">
                    <a href="/" class="txt1">
                        Quay lại
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="{!! asset('html/login/vendor/jquery/jquery-3.2.1.min.js') !!}"></script>
<!--===============================================================================================-->
<script src="{!! asset('html/login/vendor/bootstrap/js/popper.js') !!}"></script>
<script src="{!! asset('html/login/vendor/bootstrap/js/bootstrap.min.js') !!}"></script>
<!--===============================================================================================-->
<script src="{!! asset('html/login/vendor/select2/select2.min.js') !!}"></script>
<!--===============================================================================================-->
<script src="{!! asset('html/login/js/main.js') !!}"></script>

</body>
</html>