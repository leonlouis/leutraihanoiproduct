<?php
/**
 * Created by PhpStorm.
 * User: HuyDev
 * Date: 4/22/2020
 * Time: 1:03 AM
 */
Route::group(
    [
        'namespace'=>'App\Modules\Frontend\Controllers',
        'middleware' => ['web']
    ], function(){
    Route::middleware(['customer_auth'])->get('/', function () {
        echo "Hello Backend. <a href='" . url('/logout') . "'>Logout</a>";
    });
    Route::get('/login', ['as' => 'customer.login', 'uses' => 'AuthController@getLogin']);
    Route::post('/login', [ 'uses' => 'AuthController@postLogin']);
    Route::get('/logout', [ 'uses' => 'AuthController@getLogout']);
    Route::get('/test', 'HomeController@index')->name('test');
}
);