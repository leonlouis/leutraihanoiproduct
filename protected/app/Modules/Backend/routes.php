<?php
/**
 * Created by PhpStorm.
 * User: HuyDev
 * Date: 4/22/2020
 * Time: 1:03 AM
 */
Route::group(
    [
        'namespace' => 'App\Modules\Backend\Controllers',
        'middleware' => ['web']
    ], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('/login', ['as' => 'admin.login', 'uses' => 'AuthController@getLogin']);
    Route::post('/login', 'AuthController@postLogin')->name('login');
    Route::get('/logout', 'AuthController@getLogout')->name('logout');
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('category-index');
    });
}
);
