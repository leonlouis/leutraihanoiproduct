<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{

    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = [
        'fullname',
        'email',
        'password',
        'active'
    ];

    protected $guarded = [
        'id'
    ];

    protected $hidden = ['password', 'remember_token'];
}